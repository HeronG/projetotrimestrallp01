unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    lbl_titulo: TLabel;
    edt_nome: TEdit;
    edt_tipo: TEdit;
    edt_musica: TEdit;
    btn_inserir: TButton;
    btn_deletar: TButton;
    btn_atualizar: TButton;
    btn_salvar: TButton;
    lb_registros: TListBox;
    procedure btn_inserirClick(Sender: TObject);
    procedure btn_deletarClick(Sender: TObject);
    procedure btn_atualizarClick(Sender: TObject);
    procedure btn_salvarClick(Sender: TObject);
    procedure lb_registrosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  TBanda = class(TObject)
    nome: string;
    tipo:string;
    musica:string;
    end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn_atualizarClick(Sender: TObject);
var banda:TBanda;
var nome, tipo,musica: string;
begin
  nome:=edt_nome.Text;
  tipo:=edt_tipo.Text;
  musica:= edt_musica.Text;
  if (nome.Length<>0) and (tipo.Length<>0) and (musica.Length<>0) then
  begin
    banda := lb_registros.Items.Objects[lb_registros.ItemIndex] as TBanda;
    banda.nome := nome;
    banda.tipo := tipo;
    banda.musica := musica;
    lb_registros.Items[lb_registros.ItemIndex]:= edt_nome.Text ;
  end
  else
  begin
    ShowMessage('Preencha todos os campos!');
  end;
end;

procedure TForm1.btn_deletarClick(Sender: TObject);
begin
    lb_registros.DeleteSelected
end;

procedure TForm1.btn_inserirClick(Sender: TObject);
var banda:TBanda;
var nome, tipo, musica: string;
begin
  nome:=edt_nome.Text;
  tipo:=edt_tipo.Text;
  musica:= edt_musica.Text;
  if (nome.Length<>0) and (tipo.Length<>0) and (musica.Length<>0) then
  begin
    banda := TBanda.Create;
    banda.nome := nome;
    banda.tipo := tipo;
    banda.musica := musica;
    lb_registros.Items.AddObject(banda.nome, banda);
  end
  else
  begin
    ShowMessage('Preencha todos os campos!');
  end;

end;

procedure TForm1.btn_salvarClick(Sender: TObject);
var arq : TextFile;
var banda:TBanda;
var i: integer;
begin
    if lb_registros.Items.Count<>0 then
    begin
      AssignFile(arq, 'lista.txt');
      Rewrite(arq);
      for i:=0 to Pred(lb_registros.Items.Count) do
      begin
        banda := lb_registros.Items.Objects[i] as TBanda;
        writeln(arq, banda.nome+'|'+banda.tipo+'|'+banda.musica+'|');
      end;

      CloseFile(arq);
    end
    else
    begin
      ShowMessage('Nenhum item para salvar!');
    end;
end;

procedure TForm1.lb_registrosClick(Sender: TObject);
begin
     btn_deletar.Enabled:=true;
     btn_atualizar.Enabled:=true;
end;

end.
